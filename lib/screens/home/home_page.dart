import 'package:flutter/material.dart';
import 'package:notebook/constants/constants.dart';
import 'package:notebook/database/database_helper.dart';
import 'package:notebook/models/note.dart';
<<<<<<< HEAD
import 'package:notebook/provider/home_page_provider.dart';
=======
import 'package:notebook/notebook_controller/notebook_controller.dart';
>>>>>>> 779a95455e5328daf7a3e736aae0fd1851671998
import 'package:notebook/screens/drawer/drawer_page.dart';
import 'package:notebook/screens/home/widgets/app_bar_title_widget.dart';
import 'package:notebook/screens/login/login_screen.dart';
import 'package:notebook/screens/note/note_add_page.dart';
import 'package:notebook/screens/note/note_update_page.dart';
import 'package:notebook/utils/custom_toast.dart';
<<<<<<< HEAD
import 'package:notebook/utils/shere_pref.dart';
=======
>>>>>>> 779a95455e5328daf7a3e736aae0fd1851671998
import 'package:provider/provider.dart';

class HomePage extends StatefulWidget {
  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  String _greeting;
  DatabaseHelper _db;
  bool isLoading;
  List<NoteBook> noteList;
  List<NoteBook> storeNoteList;
  String noData;

  @override
  void initState() {
    super.initState();
    noData = "No note available, add new";
    noteList = [];
    storeNoteList = [];
    isLoading = true;
    _db = DatabaseHelper();
    setPref();
    greetings();
    fetchNoteList();
  }

  void setPref() async {
    await Prefs.loadPref();
  }

  Future<void> fetchNoteList() async {
    var provider = Provider.of<HomePageProvider>(context, listen: false);
    try {
      var notes = await _db.fetchNoteList();
      if (notes.length > 0) {
<<<<<<< HEAD
        provider.notebooks = notes;
        /*  setState(() {
          noteList.addAll(notes);
          storeNoteList.addAll(notes);
          isLoading = false;
        });*/
      }

      provider.isLoading = false;
    } catch (error) {
      provider.isLoading = false;
      /* setState(() {
        noteList = [];
        isLoading = false;
      });*/
=======
        Provider.of<HomePageProvider>(context, listen: false).notebooks = notes;
      }
      provider.isLoading = false;
    } catch (error) {
      provider.isLoading = false;
>>>>>>> 779a95455e5328daf7a3e736aae0fd1851671998
    }
  }

  void greetings() {
    var timeOfDay = DateTime.now().hour;

    if (timeOfDay >= 0 && timeOfDay < 6) {
      _greeting = 'Good Night';
    } else if (timeOfDay >= 0 && timeOfDay < 12) {
      _greeting = 'Good Morning';
    } else if (timeOfDay >= 12 && timeOfDay < 16) {
      _greeting = 'Good Afternoon';
    } else if (timeOfDay >= 16 && timeOfDay < 21) {
      _greeting = 'Good Evening';
    } else if (timeOfDay >= 21 && timeOfDay < 24) {
      _greeting = 'Good Night';
    }
  }

  Future<void> showMenuSelection(String value, int id, NoteBook mBook) async {
    switch (value) {
      case 'Delete':
        setState(() {
          isLoading = true;
        });
        onDelete(id);
        break;

      case 'Edit':
        bool isUpdated = await Navigator.push(
          context,
          MaterialPageRoute(builder: (context) {
            return NoteUpdatePage(
              noteBook: mBook,
            );
          }),
        );

        if (isUpdated) {
          setState(() {
            isLoading = true;
          });
          noteList = [];
          fetchNoteList();
        }
        break;
    }
  }

  void onDelete(int id) async {
    int isDeleted = await _db.deleteNote(id);
    if (isDeleted == 1) {
      CustomToast.toast('Note deleted');
      setState(() {
        isLoading = true;
      });
      noteList = [];
      fetchNoteList();
    } else {
      CustomToast.toast('Note not deleted');
      setState(() {
        isLoading = false;
      });
    }
  }

  void filterSearchResult(String query) {
    noteList.clear();
    if (query.isNotEmpty) {
      List<NoteBook> newList = [];
      for (NoteBook noteBook in storeNoteList) {
        if (noteBook.title.toLowerCase().contains(query.toLowerCase()) ||
            noteBook.content.toLowerCase().contains(query.toLowerCase()) ||
            noteBook.date.toLowerCase().contains(query.toLowerCase())) {
          newList.add(noteBook);
        }
      }

      if (newList.length <= 0) {
        setState(() {
          noData = "No data found";
        });
      } else {
        setState(() {
          noteList.addAll(newList);
        });
      }
    } else {
      setState(() {
        noteList.addAll(storeNoteList);
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      floatingActionButton: Container(
        margin: EdgeInsets.only(right: 20, bottom: 20),
        child: FloatingActionButton(
            elevation: 0.0,
            child: Icon(Icons.add),
            backgroundColor: kColorPrimary,
            onPressed: () async {
              bool isAdded = await Navigator.push(
                context,
                MaterialPageRoute(builder: (context) {
                  return NoteAddPage();
                }),
              );

              if (isAdded == true) {
                fetchNoteList();
              }
            }),
      ),
      body: Scaffold(
          appBar: AppBar(
            title: AppBarTitleWidget(
              title: 'Notebook',
              subTitle: '-365',
            ),
          ),
          drawer: Drawer(
            child: DrawerPage(),
          ),
          body: Consumer<HomePageProvider>(
<<<<<<< HEAD
            builder: (_, mProvider, __) {
=======
            builder: (_, provider, __) {
>>>>>>> 779a95455e5328daf7a3e736aae0fd1851671998
              return SingleChildScrollView(
                child: Padding(
                  padding: EdgeInsets.symmetric(vertical: 20),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Padding(
                        padding: const EdgeInsets.symmetric(horizontal: 20),
                        child: Row(
                          children: <Widget>[
<<<<<<< HEAD
                            InkWell(
                              onTap: () {
                                Prefs.clearPref();
                                Navigator.of(context).pushAndRemoveUntil(
                                    MaterialPageRoute(
                                        builder: (context) => LoginScreen()),
                                    (route) => false);
                              },
                              child: Icon(
                                Icons.menu_book,
                                size: 40,
                              ),
=======
                            Icon(
                              Icons.menu_book,
                              size: 40,
>>>>>>> 779a95455e5328daf7a3e736aae0fd1851671998
                            ),
                            SizedBox(
                              width: 10,
                            ),
                            Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: <Widget>[
                                Text(
                                  'Hello' + ' Ebrahim Joy,',
                                  style: Theme.of(context)
                                      .textTheme
                                      .headline6
                                      .copyWith(
                                        fontWeight: FontWeight.w400,
                                      ),
                                ),
                                _greeting != null
                                    ? Text(
                                        _greeting,
                                        style: TextStyle(
                                          color: Colors.grey,
                                          fontSize: 14,
                                          fontFamily: 'NunitoSans',
                                          fontWeight: FontWeight.w400,
                                        ),
                                      )
                                    : Container(),
                              ],
                            ),
                          ],
                        ),
                      ),
                      Padding(
                        padding: EdgeInsets.symmetric(horizontal: 10),
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                            SizedBox(
                              height: 25,
                            ),
                            Container(
                              padding: EdgeInsets.only(
                                  top: 0, bottom: 0, left: 15, right: 15),
                              height: 55,
                              child: TextField(
                                onChanged: (value) {
                                  filterSearchResult(value);
                                },
                                // controller: _editingController,
                                decoration: InputDecoration(
                                  labelText: 'search by title...',
                                  prefixIcon: Icon(Icons.search),
                                  fillColor: kColorLight,
                                  enabledBorder: UnderlineInputBorder(
                                    borderSide: BorderSide(color: Colors.white),
                                  ),
                                  border: UnderlineInputBorder(
                                    borderSide: BorderSide(color: Colors.white),
                                    borderRadius:
                                        BorderRadius.all(Radius.circular(8.0)),
                                  ),
                                  focusedBorder: OutlineInputBorder(
                                      borderRadius: BorderRadius.all(
                                          Radius.circular(8.0)),
                                      borderSide:
                                          BorderSide(color: kColorPrimary)),
                                  filled: true,
                                  contentPadding: EdgeInsets.only(
                                      bottom: 10.0, left: 10.0, right: 10.0),
                                ),
                              ),
<<<<<<< HEAD
                            ),
                            SizedBox(
                              height: 10,
                            ),
                            !mProvider.isLoading
                                ? mProvider.notebooks.contains(null) ||
                                        mProvider.notebooks.length <= 0
=======
                            ),
                            SizedBox(
                              height: 10,
                            ),
                            !provider.isLoading
                                ? provider.notebooks.contains(null) ||
                                        provider.notebooks.length <= 0
>>>>>>> 779a95455e5328daf7a3e736aae0fd1851671998
                                    ? Container(
                                        child: Center(child: Text(noData)))
                                    : ListView.separated(
                                        separatorBuilder: (context, index) =>
                                            SizedBox(
                                          height: 5,
                                        ),
                                        shrinkWrap: true,
                                        physics: NeverScrollableScrollPhysics(),
<<<<<<< HEAD
                                        itemCount: mProvider.notebooks.length,
                                        itemBuilder: (context, index) {
                                          return noteListItem(
                                              mProvider.notebooks[index]);
=======
                                        itemCount: provider.notebooks.length,
                                        itemBuilder: (context, index) {
                                          return noteListItem(
                                              provider.notebooks[index]);
>>>>>>> 779a95455e5328daf7a3e736aae0fd1851671998
                                        },
                                      )
                                : Center(
                                    child: CircularProgressIndicator(
                                      valueColor: AlwaysStoppedAnimation<Color>(
                                          kColorPrimary),
                                    ),
                                  )
                          ],
                        ),
                      ),
                    ],
                  ),
                ),
              );
            },
          )),
    );
  }

  Padding noteListItem(NoteBook noteBook) {
    return Padding(
      padding: EdgeInsets.only(left: 15, right: 15, top: 0, bottom: 0),
      child: Container(
        decoration: BoxDecoration(
          border: Border.all(
            color: kColorLight,
          ),
          color: Colors.white,
          borderRadius: BorderRadius.circular(8),
        ),
        child: Padding(
          padding: const EdgeInsets.all(8.0),
          child: Row(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Expanded(
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Text(
                      noteBook.title,
                      style: Theme.of(context)
                          .textTheme
                          .subtitle1
                          .copyWith(fontWeight: FontWeight.w700),
                    ),
                    Text(
                      noteBook.content,
                      maxLines: 1,
                      overflow: TextOverflow.ellipsis,
                      style: Theme.of(context).textTheme.subtitle2,
                    ),
                    Text(
                      noteBook.date,
                      maxLines: 1,
                      overflow: TextOverflow.ellipsis,
                      style: Theme.of(context).textTheme.bodyText2,
                    ),
                  ],
                ),
              ),
              PopupMenuButton<String>(
                padding: EdgeInsets.zero,
                icon: Icon(Icons.more_vert),
                onSelected: (value) {
                  showMenuSelection(value, noteBook.id, noteBook);
                },
                itemBuilder: (BuildContext context) => <PopupMenuEntry<String>>[
                  const PopupMenuItem<String>(
                      value: 'Edit',
                      child: ListTile(
                          leading: Icon(Icons.edit), title: Text('Update'))),
                  const PopupMenuItem<String>(
                      value: 'Delete',
                      child: ListTile(
                          leading: Icon(Icons.delete), title: Text('Delete'))),
                ],
              )
            ],
          ),
        ),
      ),
    );
  }
}
