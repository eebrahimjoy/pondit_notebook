import 'package:flutter/foundation.dart';

class MyData extends ChangeNotifier {
  String data = "myData";
  int number = 10;

  void changedString(String newString) {
    data = newString;
    notifyListeners();
  }
}
