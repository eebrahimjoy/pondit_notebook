import 'package:flutter/foundation.dart';
import 'package:notebook/models/note.dart';

class HomePageProvider extends ChangeNotifier {
  bool _isLoading = true;
  List<NoteBook> _notebooks = [];

  bool get isLoading => _isLoading;

  set isLoading(bool value) {
    _isLoading = value;
  }

  List<NoteBook> get notebooks => _notebooks;

  set notebooks(List<NoteBook> value) {
    _notebooks = value;
    notifyListeners();
  }
}
